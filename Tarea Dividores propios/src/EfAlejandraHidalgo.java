import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;
/**
 * 
 * @author Alejandra Hidalgo tarea divisores propios
 *
 */
// no existe reporte en findbugs
public class EfAlejandraHidalgo { // NOPMD by jandy on 9/11/17 23:46
	/**
	 * 
	 * metodo principal
	 */
		public static void main(final String[] args) {
			boolean repetir=true;
			int numero=0;
			final Scanner teclado=new Scanner(System.in);
			//validacion de numero
			while(repetir) {
				System.out.print("Ingrese un numero: "); // NOPMD by jandy on 9/11/17 23:39
				try{
					numero=teclado.nextInt();
					repetir=false;
				}catch(InputMismatchException ex){
					teclado.nextLine();
					System.err.println("La entrada ingresada no es un numero!"); // NOPMD by jandy on 9/11/17 23:44
				}
			}
			final int resultado=divisorPropio(numero);
			if(resultado==-1){
				System.out.print("Es numero primo"); // NOPMD by jandy on 9/11/17 23:39
			}
			else {
				if(numero!=1) {
				System.out.print(resultado); // NOPMD by jandy on 9/11/17 23:44
				}
			}
		}
		/**
		 * 
		 * 
		 * Devuelve un divisor propio aleatorio 
		 */
		public static int divisorPropio(final int numero){
			int div=-1;
			final ArrayList<Integer> divisores=new ArrayList<Integer>();
			if(numero>1){
				for(int i=2; i<numero; i++) {
					if(numero%i==0) {
						div=i;
						divisores.add(i);					
					}			
				}
				if(div!=-1) {
					div=divisores.get(new Random().nextInt(divisores.size()));
				}
			}else {
				div=1;
				System.out.print("No es primo, ni factor propio"); // NOPMD by jandy on 9/11/17 23:42
			}
			return div;
		}
	}


