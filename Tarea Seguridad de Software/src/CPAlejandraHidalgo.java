import java.util.*;
import java.io.*;
/**
 * 
 * @author Alejandra Hidalgo tarea numeros primos
 *
 */
//no existe reporte en findbugs
public class CPAlejandraHidalgo {
	/**
	* 
	*
	* Metodo principal
	*/
	public static void main(final String[] args) {
		int numero_min=2;
		final Scanner entrada=new Scanner(System.in);
		PrintWriter out=null;
		int num=0;
		boolean repetir=true;
		
		try {
			out=new PrintWriter(new File("salida_primos.txt"));
			while(repetir) {
				System.out.print("Ingrese numero: "); // NOPMD by jandy on 9/11/17 21:32
				num=entrada.nextInt();
				if(num>=numero_min) {
					repetir=false;
				}else {
					System.out.println("El numero que ingreso e menor que dos, ingrese de denuevo"); // NOPMD by jandy on 9/11/17 21:31
				}
			}
		}catch(InputMismatchException ex){
			System.out.println("ERROR!!!!, entrada no numerica"); // NOPMD by jandy on 9/11/17 21:32
			entrada.nextLine();
		}catch(FileNotFoundException ex){
			System.out.println("Error, archivo no encontrado."); // NOPMD by jandy on 9/11/17 21:19
		}
		
		for(int j=2; j<=num; j++) {
			if(soyprimo(j)) {
				System.out.println(j); // NOPMD by jandy on 9/11/17 21:33
				if(out!=null){
					out.println(j);
				}
			}
		}
		if(out!=null) {
			out.close();
		}
		
	}
	
	/**
	* 
	*
	* Metodo que verifica si es primo o no lo es
	*/
	public static boolean soyprimo(final int numero) {
		boolean primo=true;
		for(int i=2; i<numero; i++) {
			if(numero%i==0) {
				primo=false;
			}
		}
	return primo;
	}
	

}

/*Ingrese numero: 100 // NOPMD by jandy on 9/11/17 21:46
  
2
3
5
7
11
13
17
19
23
29
31
37
41
43
47
53
59
61
67
71
73
79
83
89
97
*/
/*10000000*/
